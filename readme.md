Ryan's Ferris Sweep QMK Layout
===============================================

![Rough layout image](https://ryan.himmelwright.net/img/posts/qmk-flash-ferris-sweep/sweep_layout.png)

This repo contains the QMK keyboard layout files for my Ferris Sweep (half
swept) build. I have written in more detail about this layout, along with
building and flashing the keyboard with it. Those posts can be found here:

- [Building a 34 Key Layout](https://ryan.himmelwright.net/post/building-34-key-layout/)
- [Building my Ferris Sweep](https://ryan.himmelwright.net/post/building-my-ferris-sweep/)
- [Flashing my Ferris Sweep using QMK](https://ryan.himmelwright.net/post/qmk-flashing-ferris-sweep/)

*Note, the layout may differ slightly from what was origonally written in the
posts, and/or what is depicted in any images as I tweak my configuration over
time.
